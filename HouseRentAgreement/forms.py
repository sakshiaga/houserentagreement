from django import forms
from django.core.validators import FileExtensionValidator
from request.models import Documents
from django.contrib.admin.widgets import AdminDateWidget


class ContractDetailsForm(forms.Form):
    rent_payment_date_choices = [
        ("1st of every month", "1st of every month"),
        ("2nd of every month", "2nd of every month"),
        ("3rd of every month", "3rd of every month"),
        ("4th of every month", "4th of every month"),
        ("5th of every month", "5th of every month"),
        ("6th of every month", "6th of every month"),
        ("7th of every month", "7th of every month"),
        ("8th of every month", "8th of every month"),
        ("9th of every month", "9th of every month"),
        ("10th of every month", "10th of every month")
    ]
    agreement_start_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datetimepicker'})
)
    monthly_rent = forms.IntegerField(min_value=1000)
    rent_payment_date = forms.ChoiceField(choices=rent_payment_date_choices, initial="5th of every month")
    security_amount = forms.IntegerField(min_value=0)
    notice_period = forms.ChoiceField(choices=[("1 month", "1 month"), ("2 months", "2 months"), ("3 months", "3 months")])
    rent_increase_perc = forms.IntegerField(min_value=0, max_value=50)

    def clean_agreement_start_date(self):
        return self.cleaned_data["agreement_start_date"].strftime("%m/%d/%Y")
    
    def save(self, request_id):
        from request.models import Request
        r = Request.objects.get(id=request_id)
        r.data.update(self.cleaned_data)
        r.save()

class DocumentForm(forms.Form):
    owner_aadhar_card = forms.FileField(label='Owner Aadhar Card',
                                        validators=[FileExtensionValidator(allowed_extensions=["pdf", "jpeg", "jpg", "png"])])
    owner_pan_card = forms.FileField(label='Owner PAN Card',
                                     validators=[FileExtensionValidator(allowed_extensions=["pdf", "jpeg", "jpg", "png"])])
    owner_photograph = forms.FileField(label='Owner Photograph',
                                       validators=[FileExtensionValidator(allowed_extensions=["pdf", "jpeg", "jpg", "png"])])
    tenant_aadhar_card = forms.FileField(label='Tenant Aadhar Card',
                                         validators=[FileExtensionValidator(allowed_extensions=["pdf", "jpeg", "jpg", "png"])])
    tenant_pan_card = forms.FileField(label='Tenant PAN Card',
                                      validators=[FileExtensionValidator(allowed_extensions=["pdf", "jpeg", "jpg", "png"])])
    tenant_photograph = forms.FileField(label='Tenant Photograph',
                                        validators=[FileExtensionValidator(allowed_extensions=["pdf", "jpeg", "jpg", "png"])])
    
    def save(self, request_id):
        from request.models import Request
        r = Request.objects.get(id=request_id)
        document_dict = {}
        for file_name, uploaded_file in self.files.items():
            i = Documents.objects.create(file=uploaded_file)
            document_dict[file_name] = i.id
        r.data.update(document_dict)
        r.save()
from io import BytesIO
import os
from django.shortcuts import HttpResponse, redirect, render
from django.views import View
from HouseRentAgreement import settings
from request import models
from request.models import Request
from .forms import ContractDetailsForm, DocumentForm

import json
import pdfkit
from django.template.loader import get_template

from django.http import HttpResponse



def index(request):
    if request.method == "GET":
        return render(request, 'index.html')
    else:
        context ={
            'city' : request.POST.get("city")
        }
        i = Request(data=context)
        i.save()
        return redirect("/%s/basicdetails/" % (i.id,))
        
def basicdetails(request, request_id):
    indian_states = [
        "Andhra Pradesh",
        "Arunachal Pradesh",
        "Assam",
        "Bihar",
        "Chhattisgarh",
        "Goa",
        "Gujarat",
        "Haryana",
        "Himachal Pradesh",
        "Jharkhand",
        "Karnataka",
        "Kerala",
        "Madhya Pradesh",
        "Maharashtra",
        "Manipur",
        "Meghalaya",
        "Mizoram",
        "Nagaland",
        "Odisha",
        "Punjab",
        "Rajasthan",
        "Sikkim",
        "Tamil Nadu",
        "Telangana",
        "Tripura",
        "Uttar Pradesh",
        "Uttarakhand",
        "West Bengal"
    ]
    i = Request.objects.get(id=request_id)

    if request.method == "POST":
        context=request.POST.dict()
        context.pop("csrfmiddlewaretoken")
        i.data.update(context)
        i.save()
        return redirect("/%s/landlorddetails/" % (i.id,))
    return render(request,'basicdetails.html', {"request_id": request_id, "request_data": i.data, "states": indian_states})

def landlorddetails(request, request_id):
    i = Request.objects.get(id=request_id)

    if request.method == "POST":
        context=request.POST.dict()
        context.pop("csrfmiddlewaretoken")
        i.data.update(context)
        i.save()
        return redirect("/%s/tenantdetails/" % (i.id,))
    return render(request, "landlorddetails.html", {"request_id": request_id, "request_data": i.data})

def tenantdetails(request,request_id):
    i = Request.objects.get(id=request_id)

    if request.method == "POST":
        context=request.POST.dict()
        i.data.update(context)
        i.save()
        return redirect("/%s/propertydetails/" % (i.id,))

    return render(request, "tenantdetails.html", {"request_id": request_id, "request_data": i.data})

def contractdetails(request, request_id):
    if request.method == "POST":
        form = ContractDetailsForm(request.POST)
        if form.is_valid():
            form.save(request_id=request_id)
            return redirect("/%s/uploaddocuments/" % (request_id,))
        else:
            return render(request, 'contractdetails.html', {'form': form, "request_id": request_id})

    form = ContractDetailsForm()
    return render(request, 'contractdetails.html', {'form': form, "request_id": request_id})

def propertydetails(request,request_id):
    i = Request.objects.get(id=request_id)
    if request.method == "POST":
        context=request.POST.dict()
        context.pop("csrfmiddlewaretoken")
        i.data.update(context)
        i.save()
        return redirect("/%s/contractdetails/" % (i.id,))

    return render(request,'propertydetails.html', {"request_id": request_id, "request_data": i.data})


def uploaddocuments(request, request_id):
    i = Request.objects.get(id=request_id)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request_id=request_id)
            return redirect("/%s/document_uploaded/" % (i.id,))
    else:
        form = DocumentForm()
    return render(request, 'uploaddocuments.html', {'form': form, "request_id": request_id})

def document_uploaded(request,request_id ):
    i = Request.objects.get(id=request_id)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request_id=request_id)
            return redirect("/%s/rentagreement/" % (i.id,))
    return render(request, 'document_uploaded.html',{"request_id": request_id})


def rentagreement(request, request_id):
    try:
        request_obj = Request.objects.get(id=request_id)
        
        request_data = request_obj.data
        
        landlord_name = request_data.get('landlord1_fullname')
        tenant_name = request_data.get('tenant1_fullname')
        property_address = request_data.get('property_address')
        start_date = request_data.get('agreement_start_date')
        end_date = request_data.get('end_date')
        rent_amount = request_data.get('rent_amount')

        return render(request, 'rentagreement.html', {
            'landlord1_fullname': landlord_name,
            'tenant1_fullname': tenant_name,
            'property_address': property_address,
            'agreement_start_date': start_date,
            'end_date': end_date,
            'rent_amount': rent_amount,
            "request_id": request_id,
        })
    except Request.DoesNotExist:
        return render(request, 'error.html', {'message': 'Request not found'})



def downloadpdf(request, request_id):
    template = get_template('rentagreement_template.html')
    i = Request.objects.get(id=request_id)
    context = i.data
    html_content = template.render(context)
    
    pdf_file = pdfkit.from_string(html_content, False)

    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="rent_agreement_%s.pdf"' % (request_id,)

    return response

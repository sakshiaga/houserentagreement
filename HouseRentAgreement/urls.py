"""HouseRentAgreement URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from logging import DEBUG

from django import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from HouseRentAgreement.settings import MEDIA_URL

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index , name='index'),
    path('<int:request_id>/basicdetails/', views.basicdetails , name='basicdetails'),
    path('<int:request_id>/tenantdetails/', views.tenantdetails , name='tenantdetails'),
    path('<int:request_id>/propertydetails/', views.propertydetails , name='propertydetails'),
    path('<int:request_id>/contractdetails/', views.contractdetails , name='contractdetails'),
    path('<int:request_id>/landlorddetails/', views.landlorddetails , name='landlorddetails'),
    path('<int:request_id>/document_uploaded/', views.document_uploaded, name='document_uploaded'),
    path('<int:request_id>/uploaddocuments/', views.uploaddocuments , name='uploaddocuments'),
    path('<int:request_id>/rentagreement/', views.rentagreement , name='rentagreement'),
    path('<int:request_id>/downloadpdf/', views.downloadpdf, name='downloadpdf'),

         
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
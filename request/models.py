from django.db import models

# Create your models here.


class Request(models.Model):
    # fullname = models.CharField(max_length=50, null=True)
    # phoneNumber = models.PositiveIntegerField(null=True)
    # emailId=models.EmailField(null=True)
    # state= models.CharField(max_length=50, null=True)
    # city= models.CharField(max_length=40)
    # stamp=models.PositiveIntegerField(null=True)
    # designation=models.CharField(max_length=50, null=True)  
    data = models.JSONField(null=True)
 
         
class Documents(models.Model):
    file = models.FileField(upload_to='documents/', null=True, default=None, max_length=250)